export interface Cliente {
    idPersona: number,
    nombre: string,
    apellido: string
    ci: number,
    tipoCi: string,
    nacionalidad: string,
    email: string,
    telefono: number,
    fechaNacimiento: Date,
}
