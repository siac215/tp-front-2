export interface Cabecera {
  idCabecera: number,
  idCliente: number,
  puntUti: number,
  fecha: Date,
  concepto: String
}
