import { Parametrizacion } from './parametrizacion';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Server } from './api';


@Injectable({
  providedIn: 'root'
})
export class ParametrizacionesService {
  private basePath = 'http://localhost:8080/prueba/vencimiento/';

  private headers = new Headers({ 'Content-Type': 'application/json' });
  // Define API
  apiURL = 'http://localhost:8080/prueba';

  // For Using Fake API by Using It's URL
  constructor(private http: HttpClient) {}
  private url = Server.url.baseUrl + '/vencimiento';

  private basePath1: string="/prueba/vencimiento/agregar";

  // HttpClient API get() method => Fetch employees list
   getVencimientos(): Observable<Parametrizacion> {
    return this.http.get<Parametrizacion>(this.url + '/listar')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getVencimientoById(id:number): Observable<Parametrizacion> {
    return this.http.get<Parametrizacion>(this.url + '/listar/id?idVencimiento=' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  updateVencimiento(vencimiento){
    console.log(vencimiento);
    return this.http.put<Parametrizacion>(this.url + '/modificar',vencimiento);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteVencimiento(id: number) {
    return this.http
      .delete<Parametrizacion>(this.url + '/borrar?idVencimiento=' + id)
      .pipe(map(data => data));
  }

  postvencimiento(params) {
    return this.http.post(this.url +'/agregar', params);
  }

}
