export interface Bolsa {
    idBolsa: number,
    idCliente: number,
    fecAsigPunt: Date,
    fecCadPunt: Date,
    puntAsig: number,
    puntajeUtilizado: number,
    saldoPuntos: number,
    montoOpe: number,
    estado: string
}
