export interface Parametrizacion {
    idVencimiento: number,
    fecIniVal: Date,
    fecFinVal: Date,
    diasDurac: number,
    idBolsa:number
}
