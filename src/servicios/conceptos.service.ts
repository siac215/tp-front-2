import { Concepto } from './concepto';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Server } from './api';

@Injectable({
  providedIn: 'root'
})
export class ConceptosService {

  private basePath = 'http://localhost:8080/prueba/concepto/';

  private headers = new Headers({ 'Content-Type': 'application/json' });
  // Define API
  apiURL = 'http://localhost:8080/concepto';

  // For Using Fake API by Using It's URL
  constructor(private http: HttpClient) {}
  private url = Server.url.baseUrl + '/concepto';

  private basePath1: string="/prueba/concepto/agregar";

  // HttpClient API get() method => Fetch employees list
   getConceptos(): Observable<Concepto> {
    return this.http.get<Concepto>(this.url + '/listar')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getConceptoById(id:number): Observable<Concepto> {
    return this.http.get<Concepto>(this.url + '/listar/id?idConcepto=' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

    // HttpClient API put() method => Update employee
    updateConcepto(concepto){
      console.log(concepto);
      return this.http.put<Concepto>(this.url + '/modificar',concepto);
    }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteConcepto(id: number) {
    return this.http
      .delete<Concepto>(this.url + '/borrar?idConcepto=' + id)
      .pipe(map(data => data));
  }

  postconcepto(params) {
    return this.http.post(this.url +'/agregar', params);
  }
}


