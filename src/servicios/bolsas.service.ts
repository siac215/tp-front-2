import { Cabecera } from './cabecera';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Bolsa } from './bolsa';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Server, Proceso } from './api';


@Injectable({
  providedIn: 'root'
})
export class BolsasService {
  private basePath = 'http://localhost:8080/prueba/bolsa/';

  private headers = new Headers({ 'Content-Type': 'application/json' });
  // Define API
  apiURL = 'http://localhost:8080/prueba';

  // For Using Fake API by Using It's URL
  constructor(private http: HttpClient) { }
  private url = Server.url.baseUrl + '/bolsa';

  private basePath1: string = "/prueba/bolsa/agregar";
  // For Using Fake API by Using It's URL
  //constructor(private http: HttpClient) {}

  // HttpClient API get() method => Fetch employees list
  getBolsas(): Observable<Bolsa> {
    return this.http.get<Bolsa>(this.url + '/listar')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
  postbolsa(params) {
    return this.http.post(this.url + '/agregar', params);
  }
  // HttpClient API get() method => Fetch employee
  buscarBolsaByClienteId(id: number): Observable<Bolsa> {
    return this.http.get<Bolsa>(this.url + '/listar/idCliente?idCliente=' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method => Fetch employee
  buscarBolsaByRango(r1: string, r2: string): Observable<Bolsa> {
    let params = new HttpParams().set("r1", r1).set("r2", r2); //Create new HttpParams
    return this.http.get<Bolsa>(this.url + '/listar/rango', { params: params })
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method => Fetch employee
  buscarClienteVenceCDias(x: number): Observable<Bolsa> {
    return this.http.get<Bolsa>(this.url + '/listar/dias?x=' + x)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method => Fetch employees list
  getProceso(): Observable<Proceso> {
    return this.http.get<Proceso>(this.url + '/listar/proceso')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getCabeceras(): Observable<Cabecera> {
    return this.http.get<Cabecera>(Server.url.baseUrl + '/cabecera/listar')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getClienteById(idCliente): Observable<Cabecera> {
    return this.http.get<Cabecera>(Server.url.baseUrl + '/cabecera/listar/cliente?idCliente=' + idCliente)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getCabeceraByFecha(fecha): Observable<Cabecera> {
    return this.http.get<Cabecera>(Server.url.baseUrl + '/cabecera/listar/fecha?fecha=' + fecha)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getCabeceraByConcepto(concepto): Observable<Cabecera> {
    return this.http.get<Cabecera>(Server.url.baseUrl + '/cabecera/listar/concepto?concepto=' + concepto)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  ejecutarProceso(horas): Observable<Proceso> {
    return this.http.get<Proceso>(Server.url.baseUrl + '/proceso?accion=ejecutar&horas=' + horas) ;
  }

  detenerProceso(): Observable<Proceso> {
    return this.http.get<Proceso>(Server.url.baseUrl + '/proceso?accion=cancelar&horas=0');
  }

  postUsarPuntos(params) {
    return this.http.post(Server.url.baseUrl + '/cabecera/usopuntos', params);
  }
}
