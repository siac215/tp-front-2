import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Cliente } from './cliente';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Server } from './api';
@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  private basePath = 'http://localhost:8080/prueba/persona/';

  private headers = new Headers({ 'Content-Type': 'application/json' });
  // Define API
  apiURL = 'http://localhost:8080/prueba';

  // For Using Fake API by Using It's URL
  constructor(private http: HttpClient) { }
  private url = Server.url.baseUrl + '/persona';

  private basePath1: string = "/prueba/persona/agregar";

  // HttpClient API get() method => Fetch employees list
  getClientes(): Observable<Cliente> {
    return this.http.get<Cliente>(this.url + '/listar');
  }

  // HttpClient API get() method => Fetch employee
  getClienteById(id: number): Observable<Cliente> {
    return this.http.get<Cliente>(this.url + '/listar/id?idPersona=' + id)
      .pipe(catchError(this.handleError));
  }

  // HttpClient API get() method => Fetch employee
  getClienteByNombre(nombre: string): Observable<Cliente> {
    return this.http.get<Cliente>(this.url + '/listar/nombre?nombre=' + nombre)
      .pipe(catchError(this.handleError));
  }

  // HttpClient API get() method => Fetch employee
  getClienteByApellido(apellido: string): Observable<Cliente> {
    return this.http.get<Cliente>(this.url + '/listar/apellido?apellido=' + apellido)
      .pipe(catchError(this.handleError));
  }

  // HttpClient API get() method => Fetch employee
  getClienteByFechaCumple(fecha: Date): Observable<Cliente> {
    const unix = new Date(fecha);
    let date = ("0" + unix.getUTCDate()).slice(-2);
    // current month
    let month = ("0" + (unix.getUTCMonth() + 1)).slice(-2);
    // current year
    let year = unix.getFullYear();
    // prints date in YYYY-MM-DD format
    console.log(year + "-" + month + "-" + date);
    let fecha1 = year + "/" + month + "/" + date;


    return this.http.get<Cliente>(this.url + '/listar/fechacumple?fecha=' + fecha1)
      .pipe(catchError(this.handleError));
  }
  // HttpClient API put() method => Update employee
  updateCliente(cliente) {
    console.log(cliente);
    return this.http.put<Cliente>(this.url + '/modificar', cliente);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteCliente(id: number) {
    return this.http
      .delete<Cliente>(this.url + '/borrar?idPersona=' + id)
      .pipe(map(data => data));
  }

  postcliente(params) {
    return this.http.post(this.url + '/agregar', params);
  }
}
