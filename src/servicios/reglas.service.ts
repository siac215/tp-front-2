import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {environment} from "../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, retry, catchError } from "rxjs/operators";
import { Regla } from './regla';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Server } from './api';

@Injectable({
  providedIn: 'root'
})
export class ReglasService {
  private basePath = 'http://localhost:8080/prueba/regla/';

  private headers = new Headers({ 'Content-Type': 'application/json' });
  // Define API
  apiURL = 'http://localhost:8080/prueba';

  // For Using Fake API by Using It's URL
  constructor(private http: HttpClient) {}
  private url = Server.url.baseUrl + '/regla';

  private basePath1: string="/prueba/regla/agregar";

  // HttpClient API get() method => Fetch employees list
   getReglas(): Observable<Regla> {
    return this.http.get<Regla>(this.url + '/listar/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method => Fetch employee
  getReglaById(id:number): Observable<Regla> {
    return this.http.get<Regla>(this.url + '/listar/id?idRegla=' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API put() method => Update employee
  updateRegla(regla){
    console.log(regla);
    return this.http.put<Regla>(this.url + '/modificar',regla);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  deleteRegla(id: number) {
    return this.http
      .delete<Regla>(this.url + '/borrar?idRegla=' + id)
      .pipe(map(data => data));
  }

  postregla(params) {
    return this.http.post(this.url +'/agregar', params);
  }
}
