export interface Regla {
    idRegla: number,
    limiteInfe: number,
    limiteSupe: number,
    mtoEquivPunto: number
}
