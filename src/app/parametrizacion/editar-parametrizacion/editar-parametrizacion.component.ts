import { ParametrizacionesService } from './../../../servicios/parametrizaciones.service';
import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Parametrizacion } from 'src/servicios/parametrizacion';

@Component({
  selector: 'app-editar-parametrizacion',
  templateUrl: './editar-parametrizacion.component.html',
  styleUrls: ['./editar-parametrizacion.component.scss']
})
export class EditarParametrizacionComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: ParametrizacionesService,
    private recibir: ParametrizacionesService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta
      //Para el servidor
      this.getVencimientoById(id);
  }


  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.editarServicio.updateVencimiento(this.form.value).subscribe(
      (res: any) => {
        console.log(res);
        this.router.navigate(['/vencimiento']);

      }, (error: any) => {
        console.log(error);
      }
     );
     this.router.navigate(['/vencimiento']);
  }
  editadoCorrectamente(data: Parametrizacion){
    console.log('Editado Correctamente');
    console.log(data);
    this.form.reset();
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getVencimientoById(id: number) {
    this.recibir.getVencimientoById(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
        console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }

  cargarFormulario(parametrizacion: Parametrizacion){
    console.log(parametrizacion.fecIniVal);
    const unix= new Date(parametrizacion.fecIniVal);
    let date = ("0" + unix.getDate()).slice(-2);
    // current month
    let month = ("0" + (unix.getMonth() + 1)).slice(-2);
    // current year
    let year = unix.getFullYear();
    // prints date in YYYY-MM-DD format
    console.log(year + "-" + month + "-" + date);
    let fecha =year + "-" + month + "-" + date;


    const unix2= new Date(parametrizacion.fecFinVal);
    let date2 = ("0" + unix2.getDate()).slice(-2);
    // current month
    let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
    // current year
    let year2 = unix2.getFullYear();
    // prints date in YYYY-MM-DD format
    console.log(year2 + "-" + month2 + "-" + date2);
    let fecha2 =year2 + "-" + month2 + "-" + date2;

    this.form = this.formBuilder.group(
      {
      idVencimiento: new FormControl(parametrizacion.idVencimiento),
      fecIniVal: new FormControl(fecha),
      fecFinVal: new FormControl(fecha2),
      diasDurac: new FormControl(parametrizacion.diasDurac),
      idBolsa: new FormControl(parametrizacion.idBolsa)

    });
  }


  onclickBack(){
    this.router.navigate(['/parametrizacion']);
  }


}
