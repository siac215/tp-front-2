import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarParametrizacionComponent } from './editar-parametrizacion.component';

describe('EditarParametrizacionComponent', () => {
  let component: EditarParametrizacionComponent;
  let fixture: ComponentFixture<EditarParametrizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarParametrizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarParametrizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
