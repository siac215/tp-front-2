import { ParametrizacionesService } from './../../../servicios/parametrizaciones.service';
import { Parametrizacion } from './../../../servicios/parametrizacion';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';




@Component({
  selector: 'app-parametrizacion',
  templateUrl: './parametrizacion.component.html',
  styleUrls: ['./parametrizacion.component.scss']
})
export class ParametrizacionComponent implements OnInit {
  MyDataSource: any;
  displayedColumns = ["fecIniVal", "fecFinVal", "diasDurac","idBolsa", "accion"];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router, public dataService: ParametrizacionesService) { }

  ngOnInit() {
    this.RenderDataTable();

  }

  RenderDataTable() {
    this.dataService.getVencimientos()
      .subscribe(
      res => {
       var size = Object.keys(res).length;
       var i=0;
        while(i<size){
          const unix= new Date(res[i].fecIniVal);
        let date = ("0" + unix.getDate()).slice(-2);
        // current month
        let month = ("0" + (unix.getMonth() + 1)).slice(-2);
        // current year
        let year = unix.getFullYear();
        // prints date in YYYY-MM-DD format
        console.log(year + "-" + month + "-" + date);
        let fecha =year + "-" + month + "-" + date;
        res[0].fecIniVal=fecha;

        const unix2= new Date(res[i].fecFinVal);
        let date2 = ("0" + unix2.getDate()).slice(-2);
        // current month
        let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
        // current year
        let year2 = unix2.getFullYear();
        // prints date in YYYY-MM-DD format
        console.log(year2 + "-" + month2 + "-" + date2);
        let fecha2 =year2 + "-" + month2 + "-" + date2;
        res[i].fecIniVal=fecha;
        res[i].fecFinVal=fecha2;
        i++;
        }

        this.MyDataSource = new MatTableDataSource();
        this.MyDataSource.data = res;
        this.MyDataSource.sort = this.sort;
        this.MyDataSource.paginator = this.paginator;
        console.log(this.MyDataSource.data);
      },
      error => {
        console.log('Ocurrio un error al procesar los parametros !!!' + error);
      });
  }

  onDeleteVencimiento(id: number): void {
    if (confirm('Esta seguro que desea borrar la parametrización?')) {
      this.dataService.deleteVencimiento(id).subscribe();
    }
    this.volverAlListado();
  }

  onclickAgregar(){
    this.router.navigate(['/parametrizacion/crear']);
  }

  public volverAlListado(){
    this.router.navigate(['/parametrizacion']);
  }

}
