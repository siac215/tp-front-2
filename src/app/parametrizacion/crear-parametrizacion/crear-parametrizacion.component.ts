import { ParametrizacionesService } from './../../../servicios/parametrizaciones.service';
import { Parametrizacion } from './../../../servicios/parametrizacion';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-crear-parametrizacion',
  templateUrl: './crear-parametrizacion.component.html',
  styleUrls: ['./crear-parametrizacion.component.scss']
})
export class CrearParametrizacionComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ParametrizacionesService,
    private route: ActivatedRoute,
    private router: Router) { }


  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta
    this.form = this.formBuilder.group({
      idBolsa: new FormControl(id),
      fecIniVal: new FormControl(''),
      fecFinVal: new FormControl('')
    });
  }
  public onSubmit() {
    this.servicioAgregar.postvencimiento(this.form.value).subscribe(
      (res: any) => {
        console.log(res);

      }, (error: any) => {
        console.log(error);
      }
    );
    this.volverAlListado();
  }

  public recibidoCorrectamente(data: Parametrizacion){
    console.log("Creado "+data);
    this.volverAlListado();

  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/parametrizacion']);
  }

}
