import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Cliente } from 'src/servicios/cliente';
import { ClientesService } from 'src/servicios/clientes.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  MyDataSource: any;
  selectDisabled: boolean;
  searchValue: string;
  searchValue2: string;
  searchValue3: string;

  displayedColumns = ["idPersona", "nombre", "apellido", "ci", "tipoCi", "nacionalidad", "email",
    "telefono", "fechaNacimiento", "accion"];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router, public dataService: ClientesService, private changeDetectorRefs: ChangeDetectorRef) { }

  ngOnInit() {
    this.RenderDataTable();

  }

  RenderDataTable() {
    this.dataService.getClientes()
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fechaNacimiento);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fechaNacimiento = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
          this.refresh();
        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });
  }

  onDeleteCliente(id: number): void {
    if (confirm('Esta seguro que desea borrar al cliente?')) {
      this.dataService.deleteCliente(id).subscribe(res => {
        // here fire functions that fetch the data from the api
        this.refresh(); // add this!
    });
    }
  }

  onclickAgregar() {
    this.router.navigate(['/cliente/crear']);
  }

  public volverAlListado() {
    this.router.navigate(['/cliente']);
  }
  buscarCliente(nombre) {
    this.dataService.getClienteByNombre(nombre)
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fechaNacimiento);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fechaNacimiento = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
          console.log(this.MyDataSource.data);

        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });

  }
  buscarApellido(apellido) {
    this.dataService.getClienteByApellido(apellido)
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fechaNacimiento);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fechaNacimiento = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });

  }
  buscarFecha(fecha) {
    this.dataService.getClienteByFechaCumple(fecha)
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fechaNacimiento);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fechaNacimiento = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });


  }

  public refresh() {
    this.searchValue = '';
    this.searchValue2 = '';
    this.searchValue3 = '';
    this.dataService.getClientes()
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fechaNacimiento);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fechaNacimiento = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });
  }

}
