import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ClientesService } from 'src/servicios/clientes.service';
import { Cliente } from 'src/servicios/cliente';
import { Router } from '@angular/router';
import { Directive, ElementRef, HostListener, Input } from '@angular/core';
@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  styleUrls: ['./crear-cliente.component.scss']
})
export class CrearClienteComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ClientesService,
    private router: Router) { }


  ngOnInit() {
    this.form = this.formBuilder.group({
      nombre: new FormControl(''),
      apellido: new FormControl(''),
      ci: new FormControl(''),
      tipoCi: new FormControl(''),
      nacionalidad: new FormControl(''),
      email: new FormControl(''),
      telefono: new FormControl(''),
      fechaNacimiento: new FormControl(''),
    });
  }
  public onSubmit() {
    this.servicioAgregar.postcliente(this.form.value).subscribe(
      (res: any) => {
        console.log(res);

      }, (error: any) => {
        console.log(error);
      }
    );
    this.volverAlListado();

  }
  public volverAlListado() {
    this.router.navigate(['/cliente']);
  }


}
