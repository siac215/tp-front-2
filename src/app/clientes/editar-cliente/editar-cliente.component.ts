import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';
import { Cliente } from 'src/servicios/cliente';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ClientesService } from 'src/servicios/clientes.service';


@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.scss']
})
export class EditarClienteComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: ClientesService,
    private recibir: ClientesService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta
      //Para el servidor
      this.getClienteById(id);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
     this.editarServicio.updateCliente(this.form.value).subscribe(
      (res: any) => {
        console.log(res);
        this.router.navigate(['/cliente']);

      }, (error: any) => {
        console.log(error);
      }
     );
     this.router.navigate(['/cliente']);
  }
  editadoCorrectamente(data: Cliente){
    console.log('Editado Correctamente');
    console.log(data);
    this.form.reset();
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getClienteById(id: number) {
    this.recibir.getClienteById(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
       // console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }


  cargarFormulario(cliente: Cliente){
    const unix= new Date(cliente.fechaNacimiento);
    let date = ("0" + unix.getDate()).slice(-2);

    // current month
    let month = ("0" + (unix.getMonth() + 1)).slice(-2);

    // current year
    let year = unix.getFullYear();

    // prints date in YYYY-MM-DD format
    console.log(year + "-" + month + "-" + date);

    let fecha =year + "-" + month + "-" + date;



    this.form = this.formBuilder.group({
      idPersona: new FormControl(cliente.idPersona),
      nombre: new FormControl(cliente.nombre),
      apellido: new FormControl(cliente.apellido),
      ci: new FormControl(cliente.ci),
      tipoCi: new FormControl(cliente.tipoCi),
      nacionalidad: new FormControl(cliente.nacionalidad),
      email: new FormControl(cliente.email),
      telefono: new FormControl(cliente.telefono),
      fechaNacimiento: new FormControl(fecha),
    });
  }


  onclickBack(){
    this.router.navigate(['/cliente']);
  }



}
