import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { ReservasService } from '../services/reservas.service';
import { RestaurantesService } from '../services/restaurantes.service';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.scss']
})
export class ReservasComponent implements OnInit {
  restaurantes: any;
  clientes: any;
  restauranteSeleccionado: any;
  fechaSeleccionada: any;
  clienteSeleccionado: any;
  reservas: any;
  constructor(
    private restaurantesService: RestaurantesService,
    private clienteService: ClienteService,
    private reservasService: ReservasService
  ) { }

  ngOnInit() {
    this.restaurantesService
    .obtenerRestaurantes()
    .subscribe(
      (res: any) => {
        this.restaurantes = res.restaurantes;
      }
    )

    this.clienteService.traerClientes().toPromise()
      .then(
        (res: any) => {
          this.clientes = res.clientes;
        }
      )
  }

  buscar() {
    this.reservasService.buscar(this.restauranteSeleccionado, this.fechaSeleccionada, this.clienteSeleccionado).toPromise()
    .then((res: any) => {
      this.reservas = res.reservas;
    })
  }

}
