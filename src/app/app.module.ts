import { CoreService } from './services/core.service';
import { CrearParametrizacionComponent } from './parametrizacion/crear-parametrizacion/crear-parametrizacion.component';
import { ParametrizacionComponent } from './parametrizacion/parametrizacion/parametrizacion.component';
import { Parametrizacion } from 'src/servicios/parametrizacion';
import { ParametrizacionesService } from './../servicios/parametrizaciones.service';
import { ConceptosService } from './../servicios/conceptos.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import { HomeComponent } from './home/home.component';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import { ClienteComponent } from './clientes/cliente/cliente.component';
import { MyMaterialModule } from '../app/my-material/my-material.module';
import { HttpClientModule } from '@angular/common/http';
import { CrearClienteComponent } from './clientes/crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './clientes/editar-cliente/editar-cliente.component';
import { ReglasService } from '../servicios/reglas.service';
import { CrearReglaComponent } from './reglas/crear-regla/crear-regla.component';
import { EditarReglaComponent } from './reglas/editar-regla/editar-regla.component';
import { ReglaComponent } from './reglas/regla/regla.component';

import { ConceptoComponent } from './conceptos/concepto/concepto.component';
import { CrearConceptoComponent } from './conceptos/crear-concepto/crear-concepto.component';
import { EditarConceptoComponent } from './conceptos/editar-concepto/editar-concepto.component';

import { BolsaComponent } from './bolsas/bolsa/bolsa.component';
import { ProcesoComponent } from './bolsas/proceso/proceso.component';
import { CrearBolsaComponent } from './bolsas/crear-bolsa/crear-bolsa.component';
import { BolsasService } from 'src/servicios/bolsas.service';
import { EditarParametrizacionComponent } from './parametrizacion/editar-parametrizacion/editar-parametrizacion.component';
import { UsoComponent } from './uso/uso.component';
import { GastarComponent } from './gastar/gastar.component';
import { ReservarComponent } from './reservar/reservar.component';
import { MatNativeDateModule } from '@angular/material';
import { ReservasService } from './services/reservas.service';
import { RestaurantesService } from './services/restaurantes.service';
import { ClientesService } from 'src/servicios/clientes.service';
import { ReservasComponent } from './reservas/reservas.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClienteComponent,
    CrearClienteComponent,
    EditarClienteComponent,
    ReglaComponent,
    CrearReglaComponent,
    EditarReglaComponent,
    ConceptoComponent,
    CrearConceptoComponent,
    EditarConceptoComponent,
    BolsaComponent,
    ProcesoComponent,
    CrearBolsaComponent,
    CrearParametrizacionComponent,
    EditarParametrizacionComponent,
    ParametrizacionComponent,
    UsoComponent,
    GastarComponent,
    ReservarComponent,
    ReservasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MyMaterialModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    MatNativeDateModule
  ],
  providers: [ReglasService, ConceptosService,BolsasService, ParametrizacionesService, CoreService, ReservasService, RestaurantesService, ClientesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
