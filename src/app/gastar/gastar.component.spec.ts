import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastarComponent } from './gastar.component';

describe('GastarComponent', () => {
  let component: GastarComponent;
  let fixture: ComponentFixture<GastarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
