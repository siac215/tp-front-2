import { BolsasService } from 'src/servicios/bolsas.service';
import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-gastar',
  templateUrl: './gastar.component.html',
  styleUrls: ['./gastar.component.scss']
})
export class GastarComponent implements OnInit {
  clientes:any;
  conceptos:any;
  searchValues:string;
  submitted = false;

  constructor(private _coreService: CoreService, private servicioAgregar2: BolsasService) {

   }

   gastarForm = new FormGroup({
    idPersona:new FormControl('',Validators.required),
    idConcepto: new FormControl('',Validators.required)
  });

  ngOnInit() {

    this._coreService.get_clientes().subscribe(clientes =>{
      this.clientes = clientes;
    })
    this._coreService.get_conceptos().subscribe(conceptos =>{
      this.conceptos = conceptos;
    })
  }


  public onSubmit() {
    this.servicioAgregar2.postUsarPuntos(this.gastarForm.value).subscribe(
      (res: any) => {
        console.log(res);

      }, (error: any) => {
        console.log(error);
      }
    );
    //this.volverAlListado();

  }


}
