import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../services/reservas.service';
import { RestaurantesService } from '../services/restaurantes.service';
import { ClienteService } from '../services/cliente.service';

@Component({
  selector: 'app-reservar',
  templateUrl: './reservar.component.html',
  styleUrls: ['./reservar.component.scss']
})
export class ReservarComponent implements OnInit {

  restaurantes: any;
  restauranteSeleccionado: any;
  fechaSeleccionada: any;
  horarioSeleccionado: any;
  mesasDisponibles: any;
  nombre =  '';
  cedula = '';
  apellido = '';
  mesaSeleccionada: any;
  fechaInicio: any;
  fechaFin: any;
  horarios = [
    {
      opcion: "12 a 13",
      valor: "12,13"
    },
    {
      opcion: "13 a 14",
      valor: "13,14"
    },
    {
      opcion: "14 a 15",
      valor: "14,15"
    },
    {
      opcion: "19 a 20",
      valor: "19,20"
    },
    {
      opcion: "20 a 21",
      valor: "20,21"
    },
    {
      opcion: "21 a 22",
      valor: "21,22"
    },
    {
      opcion: "22 a 23",
      valor: "22,23"
    }
  ];
  mostrarReservar = false;

  constructor(
    private restaurantesService: RestaurantesService,
    private reservasService: ReservasService,
    private clientesService: ClienteService
  ) { }

  ngOnInit() {
    this.restaurantesService
      .obtenerRestaurantes()
      .subscribe(
        (res: any) => {
          this.restaurantes = res.restaurantes;
        }
      )
  }

  buscarDisponibles() {
    console.log(this.restauranteSeleccionado);
    console.log(this.fechaSeleccionada);
    console.log(this.horarioSeleccionado);

    var horaDesde = +this.horarioSeleccionado[0].split(',')[0];
    var horaHasta = +this.horarioSeleccionado[this.horarioSeleccionado.length - 1].split(',')[1];

    var fechaInicio = new Date(this.fechaSeleccionada);
    var fechaFin = new Date(this.fechaSeleccionada);

    fechaInicio.setHours(horaDesde);
    fechaFin.setHours(horaHasta);

    this.fechaInicio = fechaInicio;
    this.fechaFin = fechaFin;

    this.reservasService.obtenerMesasDisponibles(this.restauranteSeleccionado, fechaInicio.toISOString(), fechaFin.toISOString())
      .subscribe(
        (res: any) => {
          console.log(res);
          this.mesasDisponibles = res.mesas;
          if (this.mesasDisponibles.length > 0) {
            this.mostrarReservar = true;
          } else {
            this.mostrarReservar = false;
          }
        }
      );
  }

  async reservarMesa() {
    var responseCliente: any;
    try {
      responseCliente = await this.clientesService.consultarCedula(this.cedula).toPromise();
      console.log(responseCliente);
    } catch (error) {
      console.log(responseCliente);
      console.log(error);
      responseCliente = await this.clientesService.guardarCliente(this.cedula, this.nombre, this.apellido).toPromise();
    }
    console.log(this.restauranteSeleccionado);
    this.reservasService.reservarMesa(
      this.restauranteSeleccionado,
      this.mesaSeleccionada.id,
      this.fechaInicio,
      this.fechaFin,
      responseCliente.id,
      this.mesaSeleccionada.capacidad
    )
    .toPromise()
    .then(function(res) {
      this.mostrarReservar = false;
      this.mesasDisponibles = []
      this.cedula = '';
      this.nombre = '';
      this.apellido = '';
      alert('Reservado con éxito');
    })

  }

}
