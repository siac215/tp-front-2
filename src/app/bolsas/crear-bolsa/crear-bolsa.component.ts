import { BolsasService } from '../../../servicios/bolsas.service';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Bolsa } from 'src/servicios/bolsa';
import { Router } from '@angular/router';
import { CoreService } from 'src/app/services/core.service';


@Component({
  selector: 'app-crear-bolsa',
  templateUrl: './crear-bolsa.component.html',
  styleUrls: ['./crear-bolsa.component.scss']
})
export class CrearBolsaComponent implements OnInit {
  clientes;
  datos;
  selectedValue: number;
  form: FormGroup;
  constructor(private _coreService:CoreService,private formBuilder: FormBuilder,
    private servicioAgregar: BolsasService,
    private router: Router) { }

  ngOnInit() {
    this.datos = {
      'idCliente':''
    }
    this.form = this.formBuilder.group({
        idCliente: new FormControl(''),
        fecAsigPunt:new FormControl(''),
        fecCadPunt: new FormControl(''),
        montoOpe: new FormControl(''),
    });
    this._coreService.get_clientes().subscribe(clientes=>{
      this.clientes = clientes;
      //this.datos.idCliente=this.clientes.idCliente;
      console.log(this.clientes);
    })

  }
  public onSubmit() {
    this.servicioAgregar.postbolsa(this.form.value).subscribe(
      (res: any) => {
        console.log(res);

      }, (error: any) => {
        console.log(error);
      }
    );
    this.volverAlListado();

}

  public recibidoCorrectamente(data: Bolsa){
    console.log("Creado "+data);
    this.volverAlListado();

  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/bolsa']);
  }


}
