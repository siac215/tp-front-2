import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearBolsaComponent } from './crear-bolsa.component';

describe('CrearBolsaComponent', () => {
  let component: CrearBolsaComponent;
  let fixture: ComponentFixture<CrearBolsaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearBolsaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearBolsaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
