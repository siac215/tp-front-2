import { BolsasService } from '../../../servicios/bolsas.service';
import { Bolsa } from '../../../servicios/bolsa';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';




@Component({
  selector: 'app-proceso',
  templateUrl: './proceso.component.html',
  styleUrls: ['./proceso.component.scss']
})
export class ProcesoComponent implements OnInit {
  MyDataSource: any;
  searchValue : number;
  corriendo : 'corriendo';
  cancelado : 'cancelado;'
  displayedColumns = ["idProceso","descripcion"];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _coreService: CoreService, private router: Router, public dataService: BolsasService) { }

  ngOnInit() {
    this.RenderDataTable();
  }

  RenderDataTable() {
    this.dataService.getProceso()
      .subscribe(
        res => {
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.sort = this.sort;
          this.MyDataSource.paginator = this.paginator;
          console.log(this.MyDataSource.data);
        },
        error => {
          console.log('Ocurrio un error al procesar los bolsas !!!' + error);
        });
  }

  public correrProceso(horas) {
    this.dataService.ejecutarProceso(horas)
      .subscribe(
        res => {
        },
        error => {
          console.log('Ocurrio un error al lanzar el proceso !!!' + error);
        });
  }

  public cancelarProceso() {
    this.dataService.detenerProceso()
      .subscribe(
        res => {
        },
        error => {
          console.log('Ocurrio un error al lanzar el proceso !!!' + error);
        });
  }

  public refresh() {
    this.dataService.getProceso()
      .subscribe(
        res => {
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.sort = this.sort;
          this.MyDataSource.paginator = this.paginator;
          console.log(this.MyDataSource.data);
        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });
  }


}
