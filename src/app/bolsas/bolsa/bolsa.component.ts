import { BolsasService } from '../../../servicios/bolsas.service';
import { Bolsa } from '../../../servicios/bolsa';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';



@Component({
  selector: 'app-bolsa',
  templateUrl: './bolsa.component.html',
  styleUrls: ['./bolsa.component.scss']
})
export class BolsaComponent implements OnInit {
  MyDataSource: any;
  searchValue:number;
  searchValue2:number;
  searchValue3:number;
  searchValue4:number;
  displayedColumns = ["idCliente", "fecAsigPunt","fecCadPunt",
  "puntAsig","puntajeUtilizado","saldoPuntos","montoOpe","estado","accion"];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _coreService:CoreService, private router: Router, public dataService: BolsasService) { }

  ngOnInit() {
    this.RenderDataTable();
  }

  RenderDataTable() {
    this.dataService.getBolsas()
      .subscribe(
      res => {
        var size = Object.keys(res).length;
       var i=0;
        while(i<size){
          if(res[i].fecAsigPunt!=null){
            const unix= new Date(res[i].fecAsigPunt);
          let date = ("0" + unix.getDate()).slice(-2);
          // current month
          let month = ("0" + (unix.getMonth() + 1)).slice(-2);
          // current year
          let year = unix.getFullYear();
          // prints date in YYYY-MM-DD format
          console.log(year + "-" + month + "-" + date);
          let fecha =year + "-" + month + "-" + date;

          const unix2= new Date(res[i].fecCadPunt);
          let date2 = ("0" + unix2.getDate()).slice(-2);
          // current month
          let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
          // current year
          let year2 = unix2.getFullYear();
          // prints date in YYYY-MM-DD format
          console.log(year2 + "-" + month2 + "-" + date2);
          let fecha2 =year2 + "-" + month2 + "-" + date2;
          res[i].fecAsigPunt=fecha;
          res[i].fecCadPunt=fecha2;
          i++;}else{
            res[i].fecAsigPunt=undefined;
            res[i].fecCadPunt=undefined;
            i++;
          }
        }

        this.MyDataSource = new MatTableDataSource();
        this.MyDataSource.data = res;
        this.MyDataSource.sort = this.sort;
        this.MyDataSource.paginator = this.paginator;
        console.log(this.MyDataSource.data);
      },
      error => {
        console.log('Ocurrio un error al procesar los bolsas !!!' + error);
      });
  }

  onclickAgregar(){
    this.router.navigate(['/bolsa/crear']);
  }

  onclickParam(){
    this.router.navigate(['/parametrizacion/crear']);
  }

  public volverAlListado(){
    this.router.navigate(['/bolsa']);
  }
  public buscarBolsaByClienteId(id){
    this.dataService.buscarBolsaByClienteId(id)
    .subscribe(
    res => {
      var size = Object.keys(res).length;
      var i=0;
       while(i<size){
         if(res[i].fecAsigPunt!=null){
           const unix= new Date(res[i].fecAsigPunt);
         let date = ("0" + unix.getDate()).slice(-2);
         // current month
         let month = ("0" + (unix.getMonth() + 1)).slice(-2);
         // current year
         let year = unix.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year + "-" + month + "-" + date);
         let fecha =year + "-" + month + "-" + date;

         const unix2= new Date(res[i].fecCadPunt);
         let date2 = ("0" + unix2.getDate()).slice(-2);
         // current month
         let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
         // current year
         let year2 = unix2.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year2 + "-" + month2 + "-" + date2);
         let fecha2 =year2 + "-" + month2 + "-" + date2;
         res[i].fecAsigPunt=fecha;
         res[i].fecCadPunt=fecha2;
         i++;}else{
           res[i].fecAsigPunt=undefined;
           res[i].fecCadPunt=undefined;
           i++;
         }
       }
      this.MyDataSource = new MatTableDataSource();
      this.MyDataSource.data = res;
      this.MyDataSource.sort = this.sort;
      this.MyDataSource.paginator = this.paginator;
      console.log(this.MyDataSource.data);
    },
    error => {
      console.log('Ocurrio un error al procesar los Clientes !!!' + error);
    });

  }

  public buscarBolsaByRango(rango1,rango2){
    this.dataService.buscarBolsaByRango(rango1,rango2)
    .subscribe(
    res => {
      var size = Object.keys(res).length;
      var i=0;
       while(i<size){
         if(res[i].fecAsigPunt!=null){
           const unix= new Date(res[i].fecAsigPunt);
         let date = ("0" + unix.getDate()).slice(-2);
         // current month
         let month = ("0" + (unix.getMonth() + 1)).slice(-2);
         // current year
         let year = unix.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year + "-" + month + "-" + date);
         let fecha =year + "-" + month + "-" + date;

         const unix2= new Date(res[i].fecCadPunt);
         let date2 = ("0" + unix2.getDate()).slice(-2);
         // current month
         let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
         // current year
         let year2 = unix2.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year2 + "-" + month2 + "-" + date2);
         let fecha2 =year2 + "-" + month2 + "-" + date2;
         res[i].fecAsigPunt=fecha;
         res[i].fecCadPunt=fecha2;
         i++;}else{
           res[i].fecAsigPunt=undefined;
           res[i].fecCadPunt=undefined;
           i++;
         }
       }
      this.MyDataSource = new MatTableDataSource();
      this.MyDataSource.data = res;
      this.MyDataSource.sort = this.sort;
      this.MyDataSource.paginator = this.paginator;
      console.log(this.MyDataSource.data);
    },
    error => {
      console.log('Ocurrio un error al procesar los Clientes !!!' + error);
    });

  }
  public buscarClienteVenceCDias(x){
    this.dataService.buscarClienteVenceCDias(x)
    .subscribe(
    res => {
      var size = Object.keys(res).length;
      var i=0;
       while(i<size){
         if(res[i].fecAsigPunt!=null){
           const unix= new Date(res[i].fecAsigPunt);
         let date = ("0" + unix.getDate()).slice(-2);
         // current month
         let month = ("0" + (unix.getMonth() + 1)).slice(-2);
         // current year
         let year = unix.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year + "-" + month + "-" + date);
         let fecha =year + "-" + month + "-" + date;

         const unix2= new Date(res[i].fecCadPunt);
         let date2 = ("0" + unix2.getDate()).slice(-2);
         // current month
         let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
         // current year
         let year2 = unix2.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year2 + "-" + month2 + "-" + date2);
         let fecha2 =year2 + "-" + month2 + "-" + date2;
         res[i].fecAsigPunt=fecha;
         res[i].fecCadPunt=fecha2;
         i++;}else{
           res[i].fecAsigPunt=undefined;
           res[i].fecCadPunt=undefined;
           i++;
         }
       }
      this.MyDataSource = new MatTableDataSource();
      this.MyDataSource.data = res;
      this.MyDataSource.sort = this.sort;
      this.MyDataSource.paginator = this.paginator;
      console.log(this.MyDataSource.data);
    },
    error => {
      console.log('Ocurrio un error al procesar los Clientes !!!' + error);
    });

  }

  public refresh (){
    this.searchValue = 0;
    this.searchValue2 = 0;
    this.searchValue3 = 0;
    this.searchValue4 = 0;
    this.dataService.getBolsas()
    .subscribe(
    res => {
      var size = Object.keys(res).length;
      var i=0;
       while(i<size){
         if(res[i].fecAsigPunt!=null){
           const unix= new Date(res[i].fecAsigPunt);
         let date = ("0" + unix.getDate()).slice(-2);
         // current month
         let month = ("0" + (unix.getMonth() + 1)).slice(-2);
         // current year
         let year = unix.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year + "-" + month + "-" + date);
         let fecha =year + "-" + month + "-" + date;

         const unix2= new Date(res[i].fecCadPunt);
         let date2 = ("0" + unix2.getDate()).slice(-2);
         // current month
         let month2 = ("0" + (unix2.getMonth() + 1)).slice(-2);
         // current year
         let year2 = unix2.getFullYear();
         // prints date in YYYY-MM-DD format
         console.log(year2 + "-" + month2 + "-" + date2);
         let fecha2 =year2 + "-" + month2 + "-" + date2;
         res[i].fecAsigPunt=fecha;
         res[i].fecCadPunt=fecha2;
         i++;}else{
           res[i].fecAsigPunt=undefined;
           res[i].fecCadPunt=undefined;
           i++;
         }
       }
      this.MyDataSource = new MatTableDataSource();
      this.MyDataSource.data = res;
      this.MyDataSource.sort = this.sort;
      this.MyDataSource.paginator = this.paginator;
      console.log(this.MyDataSource.data);
    },
    error => {
      console.log('Ocurrio un error al procesar los Clientes !!!' + error);
    });
  }
}
