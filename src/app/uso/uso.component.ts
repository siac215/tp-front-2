import { Cabecera } from './../../servicios/cabecera';
import { CoreService } from './../services/core.service';
import { MatPaginator, MatSort } from "@angular/material";
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BolsasService } from '../../../src/servicios/bolsas.service';



@Component({
  selector: 'app-uso',
  templateUrl: './uso.component.html',
  styleUrls: ['./uso.component.scss']
})
export class UsoComponent implements OnInit {

  MyDataSource: any;
  searchValue:string;
  searchValue2:string;
  searchValue3:string;
  displayedColumns = ["idCliente","puntUti","fecha","concepto"];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private _coreService: CoreService,public dataService: BolsasService ) { }

  ngOnInit() {
    this.RenderDataTable();
  }

  RenderDataTable() {
    this.dataService.getCabeceras()
      .subscribe(
      res => {

        var size = Object.keys(res).length;
        var i=0;
        while(i<size){
          const unix= new Date(res[i].fecha);
        let date = ("0" + unix.getDate()).slice(-2);
        // current month
        let month = ("0" + (unix.getMonth() + 1)).slice(-2);
        // current year
        let year = unix.getFullYear();
        // prints date in YYYY-MM-DD format
        console.log(year + "-" + month + "-" + date);
        let fecha =year + "-" + month + "-" + date;
        res[0].fecha=fecha;
        i++;
        }
        this.MyDataSource = new MatTableDataSource();
        this.MyDataSource.data = res;
        this.MyDataSource.sort = this.sort;
        this.MyDataSource.paginator = this.paginator;
        console.log(this.MyDataSource.data);
      },
      error => {
        console.log('Ocurrio un error al procesar los bolsas !!!' + error);
      });
  }

  buscarCliente(idCliente) {
    this.dataService.getClienteById(idCliente)
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fecha);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fecha = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
          console.log(this.MyDataSource.data);

        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });

  }

  buscarFechaUso(fecha) {
    this.dataService.getCabeceraByFecha(fecha)
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fecha);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fecha = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
          console.log(this.MyDataSource.data);

        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });

  }

  buscarConcepto(concepto) {
    this.dataService.getCabeceraByConcepto(concepto)
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fecha);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fecha = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
          console.log(this.MyDataSource.data);

        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });

  }

  public refresh() {
    this.searchValue = '';
    this.searchValue2 = '';
    this.searchValue3 = '';
    this.dataService.getCabeceras()
      .subscribe(
        res => {
          var size = Object.keys(res).length;
          var i = 0;
          while (i < size) {
            const unix = new Date(res[i].fecha);
            let date = ("0" + unix.getDate()).slice(-2);
            // current month
            let month = ("0" + (unix.getMonth() + 1)).slice(-2);
            // current year
            let year = unix.getFullYear();
            // prints date in YYYY-MM-DD format
            console.log(year + "-" + month + "-" + date);
            let fecha = year + "-" + month + "-" + date;
            res[i].fecha = fecha;
            i++;
          }
          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.paginator = this.paginator;
        },
        error => {
          console.log('Ocurrio un error al procesar los Clientes !!!' + error);
        });
  }

}


