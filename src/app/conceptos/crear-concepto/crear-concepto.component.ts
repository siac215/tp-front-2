import { ConceptosService } from './../../../servicios/conceptos.service';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Concepto } from 'src/servicios/concepto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-concepto',
  templateUrl: './crear-concepto.component.html',
  styleUrls: ['./crear-concepto.component.scss']
})
export class CrearConceptoComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ConceptosService,
    private router: Router) { }


  ngOnInit() {
    this.form = this.formBuilder.group({
        descripcion: new FormControl(''),
        puntosReq: new FormControl(''),
    });
  }
   public onSubmit() {
    console.log(this.form.value);
    this.servicioAgregar.postconcepto(this.form.value).subscribe(
      (res: any) => {
        console.log(res);

      }, (error: any) => {
        console.log(error);
      }
        );
  }

  public recibidoCorrectamente(data: Concepto){
    console.log("Creado "+data);
    this.volverAlListado();

  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/concepto']);
  }


}
