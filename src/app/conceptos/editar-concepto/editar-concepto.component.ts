import { ConceptosService } from './../../../servicios/conceptos.service';
import { Concepto } from './../../../servicios/concepto';
import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-editar-concepto',
  templateUrl: './editar-concepto.component.html',
  styleUrls: ['./editar-concepto.component.scss']
})
export class EditarConceptoComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: ConceptosService,
    private recibir: ConceptosService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta
      //Para el servidor
      this.getConceptoById(id);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
     this.editarServicio.updateConcepto(this.form.value).subscribe(
      (res: any) => {
        console.log(res);
        this.router.navigate(['/concepto']);

      }, (error: any) => {
        console.log(error);
      }
     );
     this.router.navigate(['/cliente']);
  }
  editadoCorrectamente(data: Concepto){
    console.log('Editado Correctamente');
    console.log(data);
    this.form.reset();
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getConceptoById(id: number) {
    this.recibir.getConceptoById(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
        console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }


  cargarFormulario(concepto: Concepto){
    this.form = this.formBuilder.group({
      id: new FormControl(concepto.idConcepto),
      descripcion: new FormControl(concepto.descripcion),
      puntos: new FormControl(concepto.puntosReq),
    });
  }


  onclickBack(){
    this.router.navigate(['/concepto']);
  }


}
