import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';
import { Regla } from 'src/servicios/regla';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ReglasService } from 'src/servicios/reglas.service';


@Component({
  selector: 'app-editar-regla',
  templateUrl: './editar-regla.component.html',
  styleUrls: ['./editar-regla.component.scss']
})
export class EditarReglaComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: ReglasService,
    private recibir: ReglasService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta
      //Para el servidor
      this.getReglaById(id);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.editarServicio.updateRegla(this.form.value).subscribe(
      (res: any) => {
        console.log(res);
        this.router.navigate(['/regla']);

      }, (error: any) => {
        console.log(error);
      }
     );
     this.router.navigate(['/cliente']);
  }
  editadoCorrectamente(data: Regla){
    console.log('Editado Correctamente');
    console.log(data);
    this.form.reset();
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getReglaById(id: number) {
    this.recibir.getReglaById(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
        console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }


  cargarFormulario(regla: Regla){
    this.form = this.formBuilder.group({
      id: new FormControl(regla.idRegla),
      limiteInfe: new FormControl(regla.limiteInfe),
      limiteSupe: new FormControl(regla.limiteSupe),
      mtoEquivPunto: new FormControl(regla.mtoEquivPunto),
    });
  }


  onclickBack(){
    this.router.navigate(['/regla']);
  }



}
