import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ReglasService } from 'src/servicios/reglas.service';
import { Regla } from 'src/servicios/regla';
import { Router } from '@angular/router';
@Component({
  selector: 'app-crear-regla',
  templateUrl: './crear-regla.component.html',
  styleUrls: ['./crear-regla.component.scss']
})
export class CrearReglaComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private servicioAgregar: ReglasService,
    private router: Router) { }


  ngOnInit() {
    this.form = this.formBuilder.group({
        limiteInfe: new FormControl(''),
        limiteSupe: new FormControl(''),
        mtoEquivPunto: new FormControl(''),
    });
  }
   public onSubmit() {
    this.servicioAgregar.postregla(this.form.value).subscribe(
      (res: any) => {
        console.log(res);

      }, (error: any) => {
        console.log(error);
      }
    );
    this.volverAlListado();
  }

  public recibidoCorrectamente(data: Regla){
    console.log("Creado "+data);
    this.volverAlListado();

  }
  public errorRecibido(error){
    console.log("se produjo un error ")
  }
  public volverAlListado(){
    this.router.navigate(['/regla']);
  }


}
