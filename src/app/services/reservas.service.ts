import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservasService {

  private API_URL = "http://localhost:4200/api";

  constructor(private http: HttpClient) { }

  obtenerMesasDisponibles(idRestaurante: string, fechaInicio: string, fechaFin: string) {
    var params = new HttpParams();
    params = params.append('id_restaurante', idRestaurante);
    params = params.append('fecha_inicio', fechaInicio);
    params = params.append('fecha_fin', fechaFin);
    return this.http.get(`${this.API_URL}/reservas/disponibles`, {params: params});
  }

  reservarMesa(idRestaurante, idMesa, fechaInicio, fechaFin, idCliente, cantidadSolicitada) {
    var headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Accept', 'application/json');
    return this.http.post(`${this.API_URL}/reservas/reservar`, {
      'id_restaurante': idRestaurante,
      'id_mesa': idMesa,
      'fecha_inicio': fechaInicio,
      'fecha_fin': fechaFin,
      'id_cliente': idCliente,
      'cantidad_solicitada': cantidadSolicitada
    },
    {
      headers: headers
    });
  }

  buscar(idRestaurante: string, fecha: Date, idCliente?: string) {
    var params = new HttpParams();
    params = params.append('id_restaurante', idRestaurante);
    params = params.append('fecha', fecha.toISOString().split('T')[0]);
    if(idCliente) {
      params = params.append('id_cliente', idCliente);
    }
    return this.http.get(`${this.API_URL}/reservas/buscar`, {
      params: params
    })
  }
}
