import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private API_URL = "http://localhost:4200/api";

  constructor(
    private http: HttpClient
  ) { }
  traerClientes() {
    return this.http.get(`${this.API_URL}/clientes`);
  }
  consultarCedula(cedula: string) {
    return this.http.get(`${this.API_URL}/clientes/cedula/${cedula}`);
  }

  guardarCliente(cedula: string, nombre: string, apellido: string) {
    var headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Accept', 'application/json');
    return this.http.post(`${this.API_URL}/clientes`, {
      'cedula': cedula,
      'nombre': nombre,
      'apellido': apellido
    },
    {
      headers: headers
    });
  }
}
