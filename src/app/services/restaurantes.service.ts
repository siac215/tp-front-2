import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestaurantesService {

  private API_URL = "http://localhost:4200/api";

  constructor(
    private http: HttpClient
  ) { }

  obtenerRestaurantes() {
    return this.http.get(this.API_URL + '/restaurantes');
  }
}
